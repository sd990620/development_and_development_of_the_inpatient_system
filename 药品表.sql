USE Hospitalization ;						
IF OBJECT_ID('tb_Drug') IS NOT NULL						
	DROP TABLE tb_Drug;					
GO						
CREATE TABLE tb_Drug
     (DrugID
            CHAR(4) 								
            NOT NULL								
            CONSTRAINT pk_Drug_DrugID								
	            PRIMARY KEY(DrugID)							
            CONSTRAINT ck_Drug_DrugID								
	             CHECK(DrugID LIKE '[0-9][0-9][0-9][0-9]')							
     ,Money
            Varchar(100)
            not null
     ,DrugName
            Varchar(1000)
            not null
     ,Effect
            Varchar(1000)
            not null
     ,UseTo
            Varchar(2000)
            NOT NULL
     ,HowToUse
            Varchar(2000)
            not null
     )