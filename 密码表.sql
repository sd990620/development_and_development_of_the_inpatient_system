USE Hospitalization ;					
IF OBJECT_ID('tb_password') IS NOT NULL					
	DROP TABLE tb_password;				
GO					
CREATE TABLE tb_password
       (ID
           char(10)
           not null
       ,name
           varchar(10)
           not null
       ,title
           varchar(10)
           not null
       ,password
           char(3)
           not null
       )
