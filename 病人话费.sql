USE Hospitalization ;						
IF OBJECT_ID('tb_patientcost') IS NOT NULL						
	DROP TABLE tb_patientcost;					
GO						
CREATE TABLE tb_patientcost	
  (patientID
          char(10)
          not null
          CONSTRAINT pk_patientcost_patientID								
	           PRIMARY KEY(patientID)							
          CONSTRAINT ck_patientcost_patientID							
	            CHECK(patientID	 LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')	
	            
   ,drugcost
         char(10)
         null
   ,doctorcost
         char(21)
         null
    ,livecost
         char(10)
         null
    )						

          					
