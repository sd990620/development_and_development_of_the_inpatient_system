IF OBJECT_ID('vw_Nurse') IS NOT NULL						
	DROP VIEW vw_Nurse;					
GO						
CREATE VIEW vw_Nurse
     AS	
SELECT	
        N.NurseID 
        ,n.Name 
        ,n.Task 
        ,p.Name as PatientName
        ,'����'+convert(varchar(2),DATEPART(WEEKDAY,N.RestDate ) )	AS RestDate				

        FROM			
	       tb_Nurse  AS N
	       JOIN tb_Patient  AS P ON p.PatientID = N.CAREID 		