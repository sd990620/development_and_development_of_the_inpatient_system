VERSION 5.00
Begin VB.Form 基本功能总界面 
   Caption         =   "Form1"
   ClientHeight    =   7185
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   13695
   LinkTopic       =   "Form1"
   ScaleHeight     =   7185
   ScaleWidth      =   13695
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command1 
      Caption         =   "返回"
      Height          =   615
      Left            =   3480
      TabIndex        =   5
      Top             =   4080
      Width           =   1335
   End
   Begin VB.CommandButton Cob_yao 
      Caption         =   "药品查询"
      Height          =   615
      Left            =   4320
      TabIndex        =   4
      Top             =   3000
      Width           =   2175
   End
   Begin VB.CommandButton Cob_givecost 
      Caption         =   "一键缴费"
      Height          =   735
      Left            =   4320
      TabIndex        =   3
      Top             =   1680
      Width           =   2175
   End
   Begin VB.CommandButton Cob_queyao 
      Caption         =   "挂号取药"
      Height          =   615
      Left            =   1920
      TabIndex        =   2
      Top             =   3000
      Width           =   1935
   End
   Begin VB.CommandButton Cob_search 
      Caption         =   "病人信息查询"
      Height          =   735
      Left            =   1920
      TabIndex        =   1
      Top             =   1680
      Width           =   1935
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "欢迎使用住院病人查询"
      Height          =   615
      Left            =   3360
      TabIndex        =   0
      Top             =   600
      Width           =   4095
   End
   Begin VB.Image Image1 
      Height          =   3975
      Left            =   1440
      Stretch         =   -1  'True
      Top             =   840
      Width           =   6495
   End
End
Attribute VB_Name = "基本功能总界面"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Cob_queyao_Click()
Static i As Long
i = i + 1
MsgBox "您好，你现在是第" & i & "位，请耐心等待哦", vbOKOnly, "温馨提示"
If i = 20 Then i = 0
End Sub

Private Sub Cob_yao_Click()
Dim xlApp As Excel.Application
Dim xlBook As Excel.Workbook
Dim xlsheet As Excel.Worksheet
FileName = "D:\药品资料.xlsx"
On Error Resume Next
 Set xlApp = GetObject(, "Excel.Application")     '判断Excel是否打开
    If Err.Number <> 0 Then
        Set xlApp = CreateObject("Excel.Application") '创建EXCEL对象
        xlApp.Visible = False '设置EXCEL对象不可见
    End If
        If Dir(FileName) = "" Then   '判断文件是否存在
        MsgBox FileName & "未找到！", vbOKOnly, "友情提示"
        Exit Sub
    End If
    Set xlBook = xlApp.Workbooks.Open(FileName)
    Set xlsheet = xlApp.Worksheets(1)
    xlApp.Visible = True
    xlApp.WindowState = xlMaximized
    xlBook.Activate
End Sub

Private Sub Command1_Click()
基本功能总界面.Hide
总入口.Show


End Sub

Private Sub Cob_givecost_Click()
基本功能总界面.Hide
缴费.Show
End Sub

Private Sub Cob_search_Click()
基本功能总界面.Hide
查询病人信息界面.Show
End Sub

Private Sub Form_Load()

Image1.Stretch = True
Image1.Picture = LoadPicture(App.Path & "\1.jpg")

End Sub

Private Sub Form_Resize()
Image1.Move 0, 0, Me.Width, Me.Height
End Sub


