VERSION 5.00
Begin VB.Form 护士基本界面 
   Caption         =   "Form1"
   ClientHeight    =   5040
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   10080
   LinkTopic       =   "Form1"
   ScaleHeight     =   5040
   ScaleWidth      =   10080
   StartUpPosition =   3  '窗口缺省
   Begin VB.TextBox Txb_NurseID 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   3840
      TabIndex        =   5
      Top             =   1920
      Width           =   2415
   End
   Begin VB.CommandButton Cob_return 
      Caption         =   "返回"
      Height          =   855
      Left            =   3840
      TabIndex        =   4
      Top             =   3960
      Width           =   1815
   End
   Begin VB.CommandButton Cob_task 
      Caption         =   "我的任务"
      Height          =   855
      Left            =   6600
      TabIndex        =   3
      Top             =   2760
      Width           =   1935
   End
   Begin VB.CommandButton Cob_rest 
      Caption         =   "我的休息日"
      Height          =   855
      Left            =   3840
      TabIndex        =   2
      Top             =   2760
      Width           =   1935
   End
   Begin VB.CommandButton Cob_daka 
      Caption         =   "上班打卡"
      Height          =   855
      Left            =   1080
      TabIndex        =   1
      Top             =   2760
      Width           =   1935
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "账号"
      Height          =   495
      Left            =   2400
      TabIndex        =   6
      Top             =   2040
      Width           =   1335
   End
   Begin VB.Label Lab_title 
      Alignment       =   2  'Center
      Caption         =   "        欢迎您，我们家的小护士            今天又是新的一天，要笑容满满的对待我们的病人哟"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   18
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1200
      TabIndex        =   0
      Top             =   360
      Width           =   7455
   End
End
Attribute VB_Name = "护士基本界面"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DbConnection As New ADODB.Connection
Private Sub Cob_daka_Click()
Dim sqlCommand As String
Dim recordSet As ADODB.recordSet
    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.name FROM vw_Nurse AS S where s.NurseID=" & Txb_NurseID.Text
    DbConnection.Open
    Set recordSet = DbConnection.Execute(sqlCommand)
    If Not recordSet.EOF Then
    MsgBox recordSet.Fields("name") & "打卡成功", , "温馨提示"
    Else
        MsgBox "你的输入有误，请重新输入", vbExclamation, "提示"
    End If
    DbConnection.Close
    
    
    
End Sub

Private Sub Cob_rest_Click()
Dim sqlCommand As String
Dim recordSet As ADODB.recordSet
    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.restdate FROM vw_Nurse AS S where s.NurseID=" & Txb_NurseID.Text
    DbConnection.Open
    Set recordSet = DbConnection.Execute(sqlCommand)
    If Not recordSet.EOF Then
    MsgBox "本周你是" & recordSet.Fields("restdate") & "休息哦", , "温馨提示"
    Else
        MsgBox "你的输入有误，请重新输入", vbExclamation, "提示"
    End If
    DbConnection.Close
End Sub

Private Sub Cob_return_Click()
护士基本界面.Hide
总入口.Show
Txb_NurseID.Text = " "
End Sub

Private Sub Cob_task_Click()
Dim sqlCommand As String
Dim recordSet As ADODB.recordSet
    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.task FROM vw_Nurse AS S where s.NurseID=" & Txb_NurseID.Text
    DbConnection.Open
    Set recordSet = DbConnection.Execute(sqlCommand)
    If Not recordSet.EOF Then
    MsgBox "现在你需要" & recordSet.Fields("task"), , "温馨提示"
    Else
        MsgBox "你的输入有误，请重新输入", vbExclamation, "提示"
    End If
    DbConnection.Close
    
End Sub

Private Sub Form_Load()
Cob_daka.Enabled = False
Cob_rest.Enabled = False
Cob_task.Enabled = False
End Sub

Private Sub Txb_NurseID_Change()
Cob_daka.Enabled = True
Cob_rest.Enabled = True
Cob_task.Enabled = True
End Sub
