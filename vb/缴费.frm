VERSION 5.00
Begin VB.Form 缴费 
   Caption         =   "Form1"
   ClientHeight    =   4410
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   9975
   LinkTopic       =   "Form1"
   ScaleHeight     =   4410
   ScaleWidth      =   9975
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Cob_problem 
      Caption         =   "我有疑惑"
      Height          =   495
      Left            =   7440
      TabIndex        =   8
      Top             =   3720
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   "请选择查找方式"
      Height          =   2415
      Left            =   600
      TabIndex        =   5
      Top             =   1080
      Width           =   2415
      Begin VB.OptionButton Option2 
         Caption         =   "  按病人ID查询"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1200
         Width           =   1935
      End
   End
   Begin VB.CommandButton Cob_return 
      Caption         =   "返回"
      Height          =   615
      Left            =   5280
      TabIndex        =   2
      Top             =   3720
      Width           =   1335
   End
   Begin VB.CommandButton Cob_search 
      Caption         =   "查询"
      Height          =   615
      Left            =   3480
      TabIndex        =   1
      Top             =   3720
      Width           =   1455
   End
   Begin VB.TextBox Txb_search 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   3360
      TabIndex        =   0
      Top             =   2280
      Width           =   3255
   End
   Begin VB.Label Lab_cost 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   15
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   7200
      TabIndex        =   6
      Top             =   1080
      Width           =   2415
   End
   Begin VB.Label Lab_title 
      Height          =   615
      Left            =   3480
      TabIndex        =   4
      Top             =   1440
      Width           =   3015
   End
   Begin VB.Label Lab_search 
      Alignment       =   2  'Center
      Caption         =   "请输入所要缴费的病人的相关信息"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2280
      TabIndex        =   3
      Top             =   480
      Width           =   4095
   End
End
Attribute VB_Name = "缴费"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DbConnection As New ADODB.Connection


Private Sub Cob_return_Click()
基本功能总界面.Show
缴费.Hide
Txb_search.Text = " "
Lab_cost.Caption = " "
Cob_problem.Visible = False
End Sub

Private Sub Cob_search_Click()
Dim sqlCommand As String
Dim recordSet As ADODB.recordSet
    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    If Option2.Value = True Then
    sqlCommand = "SELECT s.name,s.cost FROM tb_Patient AS S where s.PatientID = " & Txb_search.Text
    End If
    DbConnection.Open
    Set recordSet = DbConnection.Execute(sqlCommand)
    If Not recordSet.EOF Then
    Lab_cost.Caption = recordSet.Fields("Name") + "您好，您一共需要缴纳" + recordSet.Fields("Cost") + "元，有异议请查询花费表"
    Cob_problem.Visible = True
    Else
        MsgBox "未找到此人，请检查输入账号是否正确", vbExclamation, "提示"
    End If
    DbConnection.Close
End Sub

Private Sub Form_Load()
Lab_title.Visible = False
Txb_search.Visible = False
Cob_search.Visible = False
Cob_return.Visible = False
Cob_problem.Visible = False
End Sub

Private Sub Option1_Click()
If Option1.Value = True Then
           Lab_title.Visible = True
           Txb_search.Visible = True
           Lab_title.Caption = "请输入病人姓名"
           Cob_search.Visible = True
           Cob_return.Visible = True
End If
End Sub

Private Sub Option2_Click()
If Option2.Value = True Then
           Lab_title.Visible = True
           Txb_search.Visible = True
           Lab_title.Caption = "请输入病人ID"
           Cob_search.Visible = True
           Cob_return.Visible = True
End If
End Sub

Private Sub Option3_Click()
If Option3.Value = True Then
           Lab_title.Visible = True
           Txb_search.Visible = True
           Lab_title.Caption = "请输入病人身份证号"
           Cob_search.Visible = True
           Cob_return.Visible = True
End If
End Sub
