VERSION 5.00
Begin VB.Form 查询病人信息界面 
   Caption         =   "Form2"
   ClientHeight    =   7320
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   13110
   LinkTopic       =   "Form2"
   ScaleHeight     =   7320
   ScaleWidth      =   13110
   StartUpPosition =   3  '窗口缺省
   Begin VB.Frame Frame1 
      Caption         =   "基本信息"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   42
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6135
      Left            =   960
      TabIndex        =   1
      Top             =   600
      Width           =   10815
      Begin VB.TextBox txb_PatientID 
         Alignment       =   2  'Center
         Height          =   855
         Left            =   8040
         TabIndex        =   0
         Top             =   2880
         Width           =   1815
      End
      Begin VB.CommandButton Command2 
         Caption         =   "查询"
         Height          =   615
         Left            =   6720
         TabIndex        =   15
         Top             =   3840
         Width           =   1215
      End
      Begin VB.CommandButton Command1 
         Caption         =   "返回"
         Height          =   615
         Left            =   8760
         TabIndex        =   14
         Top             =   3840
         Width           =   1335
      End
      Begin VB.TextBox txb_cost 
         Alignment       =   2  'Center
         Height          =   615
         Left            =   1680
         TabIndex        =   12
         Top             =   5400
         Width           =   1695
      End
      Begin VB.TextBox txb_doctorID 
         Alignment       =   2  'Center
         Height          =   735
         Left            =   1680
         TabIndex        =   6
         Top             =   3720
         Width           =   1695
      End
      Begin VB.TextBox txb_birthdate 
         Alignment       =   2  'Center
         Height          =   855
         Left            =   1680
         TabIndex        =   5
         Top             =   2880
         Width           =   1695
      End
      Begin VB.TextBox txb_gender 
         Alignment       =   2  'Center
         Height          =   735
         Left            =   1680
         TabIndex        =   4
         Top             =   2040
         Width           =   1695
      End
      Begin VB.TextBox txb_name 
         Alignment       =   2  'Center
         Height          =   735
         Left            =   1680
         TabIndex        =   3
         Top             =   1320
         Width           =   1695
      End
      Begin VB.TextBox txb_nurseID 
         Alignment       =   2  'Center
         Height          =   735
         Left            =   1680
         TabIndex        =   2
         Top             =   4440
         Width           =   1695
      End
      Begin VB.Label Lab_shuru 
         Alignment       =   2  'Center
         Caption         =   "请输入病人ID号"
         Height          =   375
         Left            =   5760
         TabIndex        =   16
         Top             =   3120
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "花费"
         Height          =   735
         Left            =   0
         TabIndex        =   13
         Top             =   5520
         Width           =   1575
      End
      Begin VB.Label Label7 
         Caption         =   "护士"
         Height          =   615
         Left            =   0
         TabIndex        =   11
         Top             =   4440
         Width           =   1695
      End
      Begin VB.Label Label5 
         Caption         =   "住院医师"
         Height          =   735
         Left            =   0
         TabIndex        =   10
         Top             =   3720
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "出生日期"
         Height          =   855
         Left            =   120
         TabIndex        =   9
         Top             =   2880
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "性别"
         Height          =   735
         Left            =   120
         TabIndex        =   8
         Top             =   2040
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "姓名"
         Height          =   615
         Left            =   120
         TabIndex        =   7
         Top             =   1320
         Width           =   1575
      End
   End
End
Attribute VB_Name = "查询病人信息界面"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DbConnection As New ADODB.Connection
Private Sub Command1_Click()
基本功能总界面.Show
查询病人信息界面.Hide
txb_PatientID.Text = " "
txb_name.Text = " "
txb_gender.Text = " "
txb_birthdate.Text = " "
Txb_NurseID.Text = " "
txb_doctorID.Text = " "
txb_cost.Text = " "
End Sub

Private Sub Command2_Click()
Dim sqlCommand As String
Dim recordSet As ADODB.recordSet
    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.patientname, s.gender,s.birthdate,s.nursename,s.doctorname,s.cost FROM vw_Patient AS S where s.PatientID=" & txb_PatientID.Text
    DbConnection.Open
    Set recordSet = DbConnection.Execute(sqlCommand)
    If Not recordSet.EOF Then
    txb_name.Text = recordSet.Fields("PatientName")
    txb_gender.Text = recordSet.Fields("Gender")
    txb_birthdate.Text = recordSet.Fields("BirthDate")
    Txb_NurseID.Text = recordSet.Fields("NurseName")
    txb_doctorID.Text = recordSet.Fields("DoctorName")
    txb_cost.Text = recordSet.Fields("Cost") + "元"
    Else
        MsgBox "查无此人", vbExclamation, "提示"
    End If
    
    DbConnection.Close
End Sub

Private Sub Form_Load()
Command2.Enabled = False
End Sub

Private Sub txb_PatientID_Change()
Command2.Enabled = True
End Sub
