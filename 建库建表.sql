IF DB_ID('Hospitalization') IS NOT NULL						
	DROP DATABASE Hospitalization;					
CREATE DATABASE Hospitalization						
    ON							
	(NAME='DataFile_1'						
	,FILENAME='D:\Hospitalization\DataFile_1.mdf')						
LOG ON							
	(NAME='LogFile_1'						
	,FILENAME='D:\Hospitalization\LogFile_1.ldf');
USE Hospitalization
IF OBJECT_ID('tb_Patient') IS NOT NULL						
     DROP TABLE tb_Patient;
GO					
CREATE TABLE tb_Patient
    (PatientID
         char(10)
         Not NULL
         CONSTRAINT pk_Patient_PatientID				
	         PRIMARY KEY(PatientID)			
		 CONSTRAINT ck_Patient_PatientID								
	         CHECK(PatientID LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')							
	 ,Name 			
	     VARCHAR(20) 		
	     NOT NULL		
     ,Gender			
	     BIT		
	     NOT NULL		
	,BirthDate									
		DATE								
		CONSTRAINT ck_Patient_BirthDate								
			CHECK (BirthDate BETWEEN '1900-01-01' AND '2018-12-31')							
    ,Address
        Varchar(50)
        NULL
    ,ID
        CHAR(18)
        NOT NULL			
    ,Datehospital
        DATE
        CONSTRAINT ck_Patient_Datehospital							
			CHECK (Datehospital BETWEEN '1900-01-01' AND '2018-12-31')
	,NurseID
	    CHAR(5)
	    CONSTRAINT ck_Patient_NurseID								
	         CHECK(NurseID LIKE '[0-9][0-9][0-9][0-9][0-9]')
	,DoctorID
	    CHAR(4)
	    CONSTRAINT ck_Patient_DoctorID								
	         CHECK(DoctorID LIKE '[0-9][0-9][0-9][0-9]')
	,LeaveDate
	    Date
	,DrugID
	    CHAR(4)
	    CONSTRAINT ck_Patient_DrugID							
	         CHECK(DrugID LIKE '[0-9][0-9][0-9][0-9]')
	,COST
	    CHAR(7)
	    NOT NULL
	,money 
	    char(10)
	    not null
	 )
IF OBJECT_ID('tb_Nurse') IS NOT NULL						
    DROP TABLE tb_Nurse;
CREATE TABLE tb_Nurse
    (NurseID
        CHAR(5)
        CONSTRAINT pk_Nurse_NurseID				
	         PRIMARY KEY(NurseID)			
		CONSTRAINT ck_Nurse_NurseID								
	         CHECK(NurseID LIKE '[0-9][0-9][0-9][0-9][0-9]')	
	 ,Gender			
	     BIT		
	     NOT NULL
	 ,Name 			
	     VARCHAR(20) 		
	     NOT NULL	 
	 ,Task
	     VARCHAR(50)
	     NULL
	 ,RestDate
	     Date
	 ,CAREID
	     CHAR(10)
	     CONSTRAINT ck_Nurse_CAREID								
	         CHECK(CAREID LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')	
	 )
IF OBJECT_ID('tb_Doctor') IS NOT NULL						
    DROP TABLE tb_Doctor;
CREATE TABLE tb_Doctor
    (DoctorID
         CHAR(4)
	     CONSTRAINT ck_Doctor_DoctorID								
	         CHECK(DoctorID LIKE '[0-9][0-9][0-9][0-9]')
	 ,Name 			
	     VARCHAR(20) 		
	     NOT NULL
	 ,RestDate
	     Date
	 ,DrugforPatirent
	     VARCHAR(100)
	     NOT NULL
	 ,CheckPatientDate
	     DATE
	 ,CheckPatientID
	     CHAR (10)
	     CONSTRAINT ck_Doctor_CheckPatientID								
	         CHECK(CheckPatientID LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')	
	     )
IF OBJECT_ID('tb_Money') IS NOT NULL						
    DROP TABLE tb_Money;
CREATE TABLE tb_Money
     (PatientID
         CHAR(10)
         NOT NULL
         CONSTRAINT pk_Money_PatientID				
	         PRIMARY KEY(PatientID)	  
      ,DrugCost
         Char(10)
         NOT NULL
      ,OperationCost
         CHAR(10)
         NOT NULL
      ,NursingCost
         CHAR(10)
         NOT NULL
      ,HospitalizationCost
         CHAR(10)
         NOT NULL)