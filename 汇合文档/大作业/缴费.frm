VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form 缴费 
   Caption         =   "Form1"
   ClientHeight    =   5880
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   13440
   LinkTopic       =   "Form1"
   ScaleHeight     =   5880
   ScaleWidth      =   13440
   StartUpPosition =   3  '窗口缺省
   Begin MSAdodcLib.Adodc Adodc1 
      Height          =   735
      Left            =   1080
      Top             =   4320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   1296
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton Cob_liaojie 
      Caption         =   "我已了解"
      Height          =   735
      Left            =   5280
      TabIndex        =   11
      Top             =   4440
      Width           =   1335
   End
   Begin VB.CommandButton Cob_next 
      Caption         =   "排队取药"
      Height          =   495
      Left            =   9720
      TabIndex        =   9
      Top             =   4800
      Width           =   1935
   End
   Begin VB.CommandButton Cob_problem 
      Caption         =   "我有疑惑"
      Height          =   495
      Left            =   9720
      TabIndex        =   8
      Top             =   3960
      Width           =   1935
   End
   Begin VB.CommandButton Cob_return 
      Caption         =   "返回"
      Height          =   615
      Left            =   6360
      TabIndex        =   6
      Top             =   4560
      Width           =   1335
   End
   Begin VB.CommandButton Cob_search 
      Caption         =   "查询"
      Height          =   615
      Left            =   4320
      TabIndex        =   5
      Top             =   4560
      Width           =   1455
   End
   Begin VB.TextBox Txb_search 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   4320
      TabIndex        =   3
      Top             =   2640
      Width           =   3255
   End
   Begin VB.Frame Frame1 
      Caption         =   "请选择查找方式"
      Height          =   2415
      Left            =   720
      TabIndex        =   0
      Top             =   1440
      Width           =   2415
      Begin VB.OptionButton Option2 
         Caption         =   "  按病人ID查询"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   1200
         Width           =   1935
      End
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "缴费.frx":0000
      Height          =   2655
      Left            =   2760
      TabIndex        =   10
      Top             =   1440
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   4683
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label Lab_cost 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   15
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   9480
      TabIndex        =   7
      Top             =   1320
      Width           =   2415
   End
   Begin VB.Label Lab_search 
      Alignment       =   2  'Center
      Caption         =   "请输入所要缴费的病人的相关信息"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3840
      TabIndex        =   4
      Top             =   600
      Width           =   4095
   End
   Begin VB.Label Lab_title 
      BackStyle       =   0  'Transparent
      Height          =   615
      Left            =   4440
      TabIndex        =   2
      Top             =   1560
      Width           =   3015
   End
   Begin VB.Image Image1 
      Height          =   5655
      Left            =   0
      Top             =   0
      Width           =   13335
   End
End
Attribute VB_Name = "缴费"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DbConnection As New ADODB.Connection

Private Sub Cob_liaojie_Click()
Frame1.Visible = True
Lab_title.Visible = True
Lab_search.Visible = True
Txb_search.Visible = True
Lab_cost.Visible = True
Cob_search.Visible = True
Cob_return.Visible = True
Cob_problem.Visible = True
DataGrid1.Visible = False
Cob_liaojie.Visible = False
End Sub

Private Sub Cob_next_Click()

Lab_cost.Caption = ""
Dim sqlCommand As String
Dim Recordset As ADODB.Recordset
    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.cost,s.money FROM vw_Patient AS S where s.PatientID = " & Txb_search.Text
    DbConnection.Open
    Set Recordset = DbConnection.Execute(sqlCommand)
    If (Recordset.Fields("Cost") - Recordset.Fields("money")) < 0 Then
    MsgBox "扣费成功,剩余金额" & Recordset.Fields("money") - Recordset.Fields("cost") & "元", , "温馨提示"
    排队.Show
    缴费.Hide
    End If
    If (Recordset.Fields("Cost") - Recordset.Fields("money")) >= 0 Then
    MsgBox "余额不足前往充值", , "温馨提示"
    充值.Show
    缴费.Hide
    End If
       DbConnection.Close
End Sub

Private Sub Cob_problem_Click()
Frame1.Visible = False
Lab_title.Visible = False
Lab_search.Visible = False
Txb_search.Visible = False
Lab_cost.Visible = False
Cob_search.Visible = False
Cob_return.Visible = False
Cob_problem.Visible = False
DataGrid1.Visible = True
Cob_liaojie.Visible = True
Cob_next.Visible = True
Dim cn As ADODB.Connection
Dim rs As ADODB.Recordset

    Dim sql As String
    Set cn = New ADODB.Connection
    cn.Open "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    Set rs = New ADODB.Recordset
    rs.CursorLocation = adUseClient
    rs.Properties("Initial Fetch Size") = 2
    rs.Properties("Background Fetch Size") = 4
    sql = "select * from tb_patientcost where patientid = " & Txb_search
    rs.Open sql, cn, adOpenStatic, adLockOptimistic, adCmdText
    Set DataGrid1.DataSource = rs
End Sub

Private Sub Cob_return_Click()
基本功能总界面.Show
缴费.Hide
Txb_search.Text = " "
Lab_cost.Caption = " "
Cob_problem.Visible = False
End Sub

Private Sub Cob_search_Click()
Dim sqlCommand As String
Dim Recordset As ADODB.Recordset
    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    If Option2.Value = True Then
    sqlCommand = "SELECT s.patientname,s.cost,s.money FROM vw_Patient AS S where s.PatientID = " & Txb_search.Text
    End If
    DbConnection.Open
    Set Recordset = DbConnection.Execute(sqlCommand)
    If Not Recordset.EOF Then
    Lab_cost.Caption = Recordset.Fields("patientName") + "您好，您一共需要缴纳" + Recordset.Fields("Cost") + "元，当前你的余额为" + Recordset.Fields("money") & "有异议请查询花费表"
    Cob_problem.Visible = True
    Else
        MsgBox "未找到此人，请检查输入账号是否正确", vbExclamation, "提示"
    End If
    DbConnection.Close
End Sub

Private Sub Form_Load()

Cob_problem.Visible = False

Image1.Stretch = True
Image1.Picture = LoadPicture(App.Path & "\1.jpg")

Cob_next.Visible = False
DataGrid1.Visible = False
Adodc1.Visible = False
Cob_liaojie.Visible = False
           Txb_search.Visible = True
           Lab_title.Caption = "请输入病人ID"
           Cob_search.Visible = True
           Cob_return.Visible = True

End Sub


Private Sub Form_Resize()
Image1.Move 0, 0, Me.Width, Me.Height
End Sub

