VERSION 5.00
Begin VB.Form 基本信息 
   Caption         =   "Form1"
   ClientHeight    =   5790
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   12945
   LinkTopic       =   "Form1"
   ScaleHeight     =   5790
   ScaleWidth      =   12945
   StartUpPosition =   3  '窗口缺省
   Begin VB.Frame Frame1 
      Caption         =   "基本信息"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   21.75
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5295
      Left            =   1080
      TabIndex        =   0
      Top             =   240
      Width           =   10455
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         Height          =   735
         Left            =   1920
         TabIndex        =   16
         Top             =   3720
         Width           =   1935
      End
      Begin VB.CommandButton Command1 
         Caption         =   "充值"
         Height          =   615
         Left            =   3120
         TabIndex        =   14
         Top             =   4560
         Width           =   1455
      End
      Begin VB.CommandButton Cob_return 
         Caption         =   "返回"
         Height          =   615
         Left            =   5400
         TabIndex        =   13
         Top             =   4560
         Width           =   1455
      End
      Begin VB.TextBox txb_cost 
         Alignment       =   2  'Center
         Height          =   615
         Left            =   6600
         TabIndex        =   12
         Top             =   2760
         Width           =   1935
      End
      Begin VB.TextBox txb_nurseID 
         Alignment       =   2  'Center
         Height          =   615
         Left            =   6600
         TabIndex        =   10
         Top             =   1680
         Width           =   1935
      End
      Begin VB.TextBox txb_doctorID 
         Alignment       =   2  'Center
         Height          =   615
         Left            =   6600
         TabIndex        =   8
         Top             =   720
         Width           =   1935
      End
      Begin VB.TextBox txb_birthdate 
         Alignment       =   2  'Center
         Height          =   615
         Left            =   1920
         TabIndex        =   6
         Top             =   2760
         Width           =   1980
      End
      Begin VB.TextBox txb_gender 
         Alignment       =   2  'Center
         Height          =   615
         Left            =   1920
         TabIndex        =   4
         Top             =   1680
         Width           =   1935
      End
      Begin VB.TextBox txb_name 
         Alignment       =   2  'Center
         Height          =   615
         Left            =   1920
         TabIndex        =   1
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Caption         =   "余额"
         Height          =   495
         Left            =   120
         TabIndex        =   15
         Top             =   3840
         Width           =   1455
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "本次花费"
         Height          =   495
         Left            =   4920
         TabIndex        =   11
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Caption         =   "护士"
         Height          =   495
         Left            =   4680
         TabIndex        =   9
         Top             =   1800
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "住院医师"
         Height          =   375
         Left            =   4680
         TabIndex        =   7
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "出生日期"
         Height          =   615
         Left            =   240
         TabIndex        =   5
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "性别"
         Height          =   495
         Left            =   120
         TabIndex        =   3
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "姓名"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   840
         Width           =   1335
      End
   End
   Begin VB.Image Image1 
      Height          =   5415
      Left            =   0
      Top             =   240
      Width           =   12135
   End
End
Attribute VB_Name = "基本信息"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DbConnection As New ADODB.Connection

Private Sub Cob_return_Click()
基本功能总界面.Show
基本信息.Hide
 txb_name.Text = ""
    txb_gender.Text = ""
    txb_birthdate.Text = ""
    txb_nurseID.Text = ""
    txb_doctorID.Text = ""
    txb_cost.Text = ""
End Sub

Private Sub Command1_Click()
基本信息.Hide
充值.Show
End Sub

Private Sub Form_Load()

Image1.Stretch = True
Image1.Picture = LoadPicture(App.Path & "\1.jpg")
X = InputBox("请确认病人ID")
Dim sqlCommand As String
Dim Recordset As ADODB.Recordset

    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.patientname, s.gender,s.birthdate,s.nursename,s.doctorname,s.cost ,s.money FROM vw_Patient AS S where s.PatientID=" & X
    DbConnection.Open
    Set Recordset = DbConnection.Execute(sqlCommand)
    If Not Recordset.EOF Then
    txb_name.Text = Recordset.Fields("PatientName")
    txb_gender.Text = Recordset.Fields("Gender")
    txb_birthdate.Text = Recordset.Fields("BirthDate")
    txb_nurseID.Text = Recordset.Fields("NurseName")
    txb_doctorID.Text = Recordset.Fields("DoctorName")
    txb_cost.Text = Recordset.Fields("Cost") + "元"
    Text1.Text = Recordset.Fields("money") + "元"
    Else
        MsgBox "查无此人", vbExclamation, "提示"
    End If
    
    DbConnection.Close
End Sub

Private Sub Form_Resize()
Image1.Move 0, 0, Me.Width, Me.Height
End Sub


Private Sub Text4_Change()

End Sub

