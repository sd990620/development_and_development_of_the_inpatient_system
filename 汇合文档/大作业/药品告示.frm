VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form 药品告示 
   Caption         =   "Form1"
   ClientHeight    =   4815
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   10590
   LinkTopic       =   "Form1"
   ScaleHeight     =   4815
   ScaleWidth      =   10590
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command1 
      Caption         =   "返回"
      Height          =   495
      Left            =   4200
      TabIndex        =   2
      Top             =   4200
      Width           =   1575
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "药品告示.frx":0000
      Height          =   3495
      Left            =   960
      TabIndex        =   1
      Top             =   600
      Width           =   8415
      _ExtentX        =   14843
      _ExtentY        =   6165
      _Version        =   393216
      AllowUpdate     =   -1  'True
      AllowArrows     =   -1  'True
      HeadLines       =   1
      RowHeight       =   33
      WrapCellPointer =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Adodc1 
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   600
      ScaleHeight     =   795
      ScaleWidth      =   1140
      TabIndex        =   3
      Top             =   480
      Width           =   1200
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "药品查询"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   21.75
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3360
      TabIndex        =   0
      Top             =   120
      Width           =   3255
   End
   Begin VB.Image Image1 
      Height          =   4695
      Left            =   120
      Top             =   120
      Width           =   10335
   End
End
Attribute VB_Name = "药品告示"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
药品告示.Hide
药房.Show

End Sub

Private Sub Form_Load()
Adodc1.Visible = False
Image1.Stretch = True
Image1.Picture = LoadPicture(App.Path & "\1.jpg")
Dim cn As ADODB.Connection
Dim rs As ADODB.Recordset

    Dim sql As String
    Set cn = New ADODB.Connection
    cn.Open "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    Set rs = New ADODB.Recordset
    rs.CursorLocation = adUseClient
    rs.Properties("Initial Fetch Size") = 2
    rs.Properties("Background Fetch Size") = 4
    sql = "select * from tb_Drug"
    rs.Open sql, cn, adOpenStatic, adLockOptimistic, adCmdText
    Set DataGrid1.DataSource = rs
End Sub

Private Sub Form_Resize()
Image1.Move 0, 0, Me.Width, Me.Height
End Sub

