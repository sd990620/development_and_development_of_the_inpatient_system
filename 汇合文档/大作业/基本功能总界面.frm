VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form 基本功能总界面 
   Caption         =   "Form1"
   ClientHeight    =   5655
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   12345
   LinkTopic       =   "Form1"
   ScaleHeight     =   5655
   ScaleWidth      =   12345
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command1 
      Caption         =   "我的余额"
      Height          =   855
      Left            =   2760
      TabIndex        =   9
      Top             =   2040
      Width           =   1695
   End
   Begin VB.CommandButton Command3 
      Caption         =   "取药进度"
      Height          =   855
      Left            =   2760
      TabIndex        =   8
      Top             =   3240
      Width           =   1695
   End
   Begin VB.CommandButton Command6 
      Caption         =   "问题咨询"
      Height          =   855
      Left            =   7440
      TabIndex        =   6
      Top             =   3360
      Width           =   1815
   End
   Begin VB.CommandButton Command5 
      Caption         =   "退出登录"
      Height          =   735
      Left            =   5280
      TabIndex        =   5
      Top             =   4440
      Width           =   1575
   End
   Begin VB.CommandButton Command4 
      Caption         =   "返回"
      Height          =   495
      Left            =   5280
      TabIndex        =   4
      Top             =   4920
      Width           =   1575
   End
   Begin VB.PictureBox Adodc1 
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   600
      ScaleHeight     =   675
      ScaleWidth      =   1395
      TabIndex        =   7
      Top             =   4440
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "查询缴费"
      Height          =   855
      Left            =   7560
      TabIndex        =   1
      Top             =   2040
      Width           =   1695
   End
   Begin VB.CommandButton Cob_my 
      Caption         =   "我的基本信息"
      Height          =   735
      Left            =   360
      TabIndex        =   0
      Top             =   240
      Width           =   2055
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "基本功能总界面.frx":0000
      Height          =   3855
      Left            =   4080
      TabIndex        =   3
      Top             =   960
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   6800
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "温馨提示：取药排队请在交费后一系列操作后即可进行"
      Height          =   735
      Left            =   4080
      TabIndex        =   2
      Top             =   480
      Width           =   4095
   End
   Begin VB.Image Image1 
      Height          =   5655
      Left            =   0
      Top             =   0
      Width           =   12375
   End
End
Attribute VB_Name = "基本功能总界面"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DbConnection As New ADODB.Connection
Private Sub Cob_my_Click()
基本功能总界面.Hide
基本信息.Show
End Sub

Private Sub Command1_Click()
X = InputBox("请确认病人ID")
Dim sqlCommand As String
Dim Recordset As ADODB.Recordset

    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.money FROM vw_Patient AS S where s.PatientID=" & X
    DbConnection.Open
    Set Recordset = DbConnection.Execute(sqlCommand)
    MsgBox "当前你的余额还有" & Recordset.Fields("money") & "元", , "温馨提示"
    DbConnection.Close
End Sub

Private Sub Command2_Click()
基本功能总界面.Hide
缴费.Show
End Sub

Private Sub Command3_Click()
Command1.Visible = False
Command2.Visible = False
Command3.Visible = False
Command4.Visible = True
Dim cn As ADODB.Connection
Dim rs As ADODB.Recordset

    Dim sql As String
    Set cn = New ADODB.Connection
    cn.Open "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    Set rs = New ADODB.Recordset
    rs.CursorLocation = adUseClient
    rs.Properties("Initial Fetch Size") = 2
    rs.Properties("Background Fetch Size") = 4
    sql = "select patientID, ROW_NUMBEr () OVER (ORDER BY patientID ASC) as '号码' from tb_paidui"
    rs.Open sql, cn, adOpenStatic, adLockOptimistic, adCmdText
    Set DataGrid1.DataSource = rs
    DataGrid1.Visible = True
    Command5.Visible = False
    Command6.Visible = False
End Sub

Private Sub Command4_Click()
Command1.Visible = True
Command2.Visible = True
Command3.Visible = True
Command4.Visible = False
DataGrid1.Visible = False
Command5.Visible = True
Command6.Visible = True
End Sub

Private Sub Command5_Click()
基本功能总界面.Hide
登陆.Show
End Sub

Private Sub Form_Load()

Image1.Stretch = True
Image1.Picture = LoadPicture(App.Path & "\1.jpg")
Adodc1.Visible = False
DataGrid1.Visible = False
Command4.Visible = False

End Sub

Private Sub Form_Resize()
Image1.Move 0, 0, Me.Width, Me.Height
End Sub

