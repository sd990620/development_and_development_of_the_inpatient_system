VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form 取药 
   Caption         =   "Form1"
   ClientHeight    =   5445
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   12585
   LinkTopic       =   "Form1"
   ScaleHeight     =   5445
   ScaleWidth      =   12585
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton Command4 
      Caption         =   "更新列表"
      Height          =   735
      Left            =   2760
      TabIndex        =   9
      Top             =   3720
      Width           =   1095
   End
   Begin VB.CommandButton Command3 
      Caption         =   "返回"
      Height          =   495
      Left            =   7680
      TabIndex        =   7
      Top             =   4680
      Width           =   2535
   End
   Begin VB.CommandButton Command2 
      Caption         =   "查询"
      Height          =   735
      Left            =   10680
      TabIndex        =   6
      Top             =   360
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "完成取药"
      Height          =   735
      Left            =   7560
      TabIndex        =   5
      Top             =   3840
      Width           =   2655
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   735
      Left            =   7560
      TabIndex        =   4
      Top             =   1560
      Width           =   2655
   End
   Begin VB.TextBox Txb_ID 
      Alignment       =   2  'Center
      Height          =   855
      Left            =   7560
      TabIndex        =   2
      Top             =   240
      Width           =   2655
   End
   Begin VB.PictureBox Adodc1 
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   480
      ScaleHeight     =   435
      ScaleWidth      =   1140
      TabIndex        =   10
      Top             =   600
      Width           =   1200
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "取药.frx":0000
      Height          =   1815
      Left            =   1800
      TabIndex        =   0
      Top             =   1800
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   3201
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   2052
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "药品名称"
      Height          =   615
      Left            =   5520
      TabIndex        =   3
      Top             =   1800
      Width           =   1815
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "输入id查询药品"
      Height          =   615
      Left            =   5640
      TabIndex        =   8
      Top             =   480
      Width           =   1335
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "当前正在排队的人"
      Height          =   735
      Left            =   240
      TabIndex        =   1
      Top             =   2520
      Width           =   1455
   End
   Begin VB.Image Image1 
      Height          =   5295
      Left            =   0
      Top             =   120
      Width           =   12615
   End
End
Attribute VB_Name = "取药"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private DbConnection As New ADODB.Connection

Private Sub Command1_Click()
Dim sqlCommand As String
Dim Recordset As ADODB.Recordset

    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "DELETE tb_paidui WHERE PatientID = " & Txb_ID
    DbConnection.Open
    Set Recordset = DbConnection.Execute(sqlCommand)
    DbConnection.Close
    MsgBox "已通知病人来取药", , "温馨提示"
    Txb_ID.Text = ""
    Text1.Text = ""
    
    药房.Show
    取药.Hide
End Sub

Private Sub Command2_Click()
Dim sqlCommand As String
Dim Recordset As ADODB.Recordset

    DbConnection.ConnectionString = "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    sqlCommand = "SELECT s.drugID FROM tb_patient AS S where s.PatientID=" & Txb_ID
    DbConnection.Open
    Set Recordset = DbConnection.Execute(sqlCommand)
    If Not Recordset.EOF Then
      Text1.Text = Recordset.Fields("drugID")
    End If
    DbConnection.Close
End Sub

Private Sub Command3_Click()
    药房.Show
    取药.Hide
End Sub

Private Sub Command4_Click()
Dim cn As ADODB.Connection
Dim rs As ADODB.Recordset

    Dim sql As String
    Set cn = New ADODB.Connection
    cn.Open "Provider=SQLOLEDB.1;Server=(local)\SQLSEVER;Database=Hospitalization;Trusted_Connection=yes"
    Set rs = New ADODB.Recordset
    rs.CursorLocation = adUseClient
    rs.Properties("Initial Fetch Size") = 2
    rs.Properties("Background Fetch Size") = 4
    sql = "select patientID, ROW_NUMBEr () OVER (ORDER BY patientID ASC)  from tb_paidui"
    rs.Open sql, cn, adOpenStatic, adLockOptimistic, adCmdText
    Set DataGrid1.DataSource = rs
    Set rs = Nothing
End Sub

Private Sub Form_Load()
Adodc1.Visible = False

    DataGrid1.Visible = True
    Image1.Stretch = True
    Image1.Picture = LoadPicture(App.Path & "\1.jpg")
End Sub
Private Sub Form_Resize()
Image1.Move 0, 0, Me.Width, Me.Height
End Sub

