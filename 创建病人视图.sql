IF OBJECT_ID('vw_Patient') IS NOT NULL							
	DROP VIEW vw_Patient;						
GO							
CREATE VIEW vw_Patient							
	AS						
    Select
          p.PatientID as PatientID
          ,p.Name as PatientName
          ,p.BirthDate as BirthDate
          ,p.COST as cost
          ,CASE P.Gender				
	       WHEN 1 THEN '男'			
	       WHEN 0 THEN '女'			
	       END AS Gender		
          ,N.Name AS NurseName
          ,D.Name as DoctorName  
          From 
               tb_Patient as p
               JOIN tb_Nurse AS N ON N.NurseID = P.NurseID 
               JOIN tb_Doctor AS D ON D.CheckPatientID  = P.PatientID 